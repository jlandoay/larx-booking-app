let adminUser = localStorage.getItem("isAdmin");

// "window.location.search" returns the query string part of the URL
console.log(window.location.search);

// Instantiate a URLSearchParams object so we can execute methods to access specific parts of the query string
let params = new URLSearchParams(window.location.search);

// The "has" method checks if the "courseId" key exists in the URL query string
// The method returns true if the key exists
console.log(params.has('courseId'));

// The "get" method returns the value of the key passed in as an argument
console.log(params.get('courseId'));

let courseId = params.get('courseId');

let token = localStorage.getItem('token');

let courseName = document.querySelector("#courseName");
let courseDesc = document.querySelector("#courseDesc");
let coursePrice = document.querySelector("#coursePrice");
let enrollContainer = document.querySelector("#enrollContainer");

fetch(`https://larx-booking-app.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {

	console.log(data.enrollees);

	courseName.innerHTML = data.name;
	courseDesc.innerHTML = data.description;
	coursePrice.innerHTML = data.price;
	enrollContainer.innerHTML = `

		<button id="enrollButton" class="btn btn-block btn-success">Enroll</button>

	`
	
	// enroll to course
	document.querySelector("#enrollButton").addEventListener("click", () => {

		fetch('https://larx-booking-app.herokuapp.com/api/users/enroll', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				courseId: courseId
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if(data === true){

				alert("Thank you for enrolling! See you in class!");
				window.location.replace("./courses.html");

			} else {

				alert("You need to be registered first before booking a course.");
				window.location.replace("./register.html");

			}

		})

	})


	if(adminUser == "false" || !adminUser) {

		console.log("Not an adminUser");
	
	} else {

		console.log("admin access");

		fetch('https://larx-booking-app.herokuapp.com/api/users/details', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
		});

		// // get list of enrollees
		// let enrollees = data.enrollees;

		// enrollees.forEach(function(enrollee, index) {

		// 	// get user details
		// 	fetch('http:localhost:4000/api/users/details', {
		// 		headers: {
		// 			'Content-Type': 'application/json',
		// 			'Authorization': `Bearer ${token}`
		// 		}
		// 	})
		// 	.then(res => res.json())
		// 	.then(data => {

		// 		console.log(data);

		// 		let trainee = `${enrollee.userId}`;
		// 		console.log(trainee);
		// 		document.querySelector("#trainees").innerHTML +=
		// 		`
		// 			<ul class="text-left">
		// 				<li>${trainee}</li>
		// 			</ul
		// 		`
		// 	});

		// });

	};

})
