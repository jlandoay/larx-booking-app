let adminUser = localStorage.getItem("isAdmin");
let token = localStorage.getItem('token');

fetch('https://larx-booking-app.herokuapp.com/api/users/details', {
	method: 'GET',
	headers: {
		'Authorization': `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {

	console.log(data);

	// Creates a variable that will store the data to be rendered
	let userData;

	if(data.length < 1) {

		userData = "No users enrolled"

	} else {

		userData = data.map(course => {
			
			if(adminUser == "false" || !adminUser) {

				cardFooter =
					`
						<a href="./course.html?courseId=${course._id}" value="${course._id}" class="btn btn-primary text-white btn-block editButton">
							Select Course
						</a>
					`

			} else {

				cardFooter =
					`
						<a href="./course.html?courseId=${course._id}" value="${course._id}" class="btn btn-success text-white btn-block editButton">
							View Course
						</a>
						<a href="./enrollees.html?courseId=${course._id}" value="${course._id}" class="btn btn-primary text-white btn-block editButton">
							View Enrollees
						</a>
						<a href="./editCourse.html?courseId=${course._id}" value="${course._id}" class="btn btn-warning text-white btn-block editButton">
							Edit Course
						</a>
						<a href="./deleteCourse.html?courseId=${course._id}" value="${course._id}" class="btn btn-danger text-white btn-block deleteButton">
							Disable Course
						</a>
					`

			}

			return (
				`
					<div class="col-md-6 my-3">
						<div class="card">
							<div class="card-body">
								<h5 class="card-title">
									${course.name}
								</h5>
								<p class="card-text text-left">
									${course.description}
								</p>
								<p class="card-text text-right">
									₱ ${course.price}
								</p>
							</div>
							<div class="card-footer">
								${cardFooter}
							</div>
						</div>
					</div>
				`
			)

		// Replaces the comma's in the array with an empty string
		}).join("");

	}

	document.querySelector("#coursesContainer").innerHTML = userData;

})