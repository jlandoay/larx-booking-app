let token = localStorage.getItem('token');
let fullName = document.querySelector('#fullName');
let email = document.querySelector('#email');
let mobileNo = document.querySelector('#mobileNo');
let userCourses = document.querySelector('#enrollContainer');
let courseArray = [];

fetch('https://larx-booking-app.herokuapp.com/api/users/details', {
	headers: {
		'Authorization': `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {
	if(data) {
		fullName.innerHTML = `Full Name: ${data.firstName} ${data.lastName}`
		email.innerHTML = `Email Address: ${data.email}`
		mobileNo.innerHTML = `Mobile Number: ${data.mobileNo}`
	} else {
		alert("Something went wrong!");
	}

	data.enrollments.forEach(course => {
		console.log(course)
	})

	data.enrollments.forEach(course => {
		let courseId = course.courseId;
		fetch(`https://larx-booking-app.herokuapp.com/api/courses/${courseId}`, {
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {

			if(data) {
				userCourses.innerHTML +=
				`
					<div class="card text-secondary my-2">
						<div class="card-body">
							<p class="card-title text-success">${data.name}</p>
							<p class="card-title">${course.enrolledOn}</p>
							<p class="card-title">${course.status}</p>
						</div>
					</div>

				`
			} else {
				alert("Something went wrong!");
			}

		})
	})

})